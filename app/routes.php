<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('user/role', array('uses' => 'UserController@roles','as' => 'user.role'));
Route::post('user/role_update/{id}', array('uses' => 'UserController@roleUpdate','as' => 'user.role_update'));
Route::resource('role', 'RoleController');
Route::resource('user', 'UserController');
Route::get('/', function()
{
	return View::make('hello');
});