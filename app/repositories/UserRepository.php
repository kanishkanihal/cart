<?php

namespace repositories;

use repositories;
use User;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface {

	
	
	function __construct() {

	}
	
	/*
	 * (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::create()
	 */
	public function insert($data) {
		$user = new User ();
		$user->email = $data ['email'];
		$user->username = $data ['username'];
		$user->password = Hash::make ( $data ['password'] );
		$user->full_name = $data ['full_name'];
		$user->address = $data ['address'];
		$user->city = $data ['city'];
		$user->dob = $data ['dob'];
		$user->sex = $data ['sex'];
		$user->zip = $data ['zip'];
		$user->save ();
		$this->updateUserRoles($user->id, [3]);
	}

	
	/*
	 * (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::all()
	 */
	public function all() {
		$user = new User ();
		return $user->all ();
	}
	
	/*
	 * (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::delete()
	 */
	public function delete($id) {
		$user = User::find($id);
		$this->updateUserRoles($id, []);
		$user->delete();
		
	}
	/* (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::find()
	 */
	public function find($id) {
		$user = new User();
		return $user->find($id);

	}

	/* (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::update()
	 */
	public function update($id, $data) {

		$user = User::find($id);
		
		$user->email = $data ['email'];
		$user->username = $data ['username'];
		$user->full_name = $data ['full_name'];
		$user->address = $data ['address'];
		$user->city = $data ['city'];
		$user->dob = $data ['dob'];
		$user->sex = $data ['sex'];
		$user->zip = $data ['zip'];
		if(isset($data ['image'])){
			$user->image = $data ['image'];
		}
		if($data ['password'] != ""){
			$user->password = Hash::make ( $data ['password'] );
		}
		$user->save ();
	}

	/* (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::allWithRole()
	 */
	public function allWithRole($id = null) {

		return User::with(array('roles' => function($query) use ($id)
		{
			$query->select('*');
			if($id != NULL){
				$query->where('role_id', '=', $id);
			}
		}))->paginate(5);

	}

	/* (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::updateUserRoles()
	 */
	public function updateUserRoles($id, $roles) {
		$user = User::with('roles')->find($id);
		$user->roles()->detach();
		if($roles){
			$user->roles()->attach($roles);
		}
		

	}

}