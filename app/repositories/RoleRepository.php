<?php
namespace repositories;

use repositories;
use Role;

class RoleRepository implements RoleRepositoryInterface{
	
	function all() {
		return Role::all();
	}
	/* (non-PHPdoc)
	 * @see \repositories\UserRepositoryInterface::find()
	 */
	public function find($id) {
		$role = new Role();
		return $role->find($id);
	
	}
	/**
	 * (non-PHPdoc)
	 * @see \repositories\RoleRepositoryInterface::userRoles()
	 */
	public function userRoles(){
		
	}
}