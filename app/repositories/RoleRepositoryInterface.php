<?php
namespace repositories;

interface RoleRepositoryInterface{
	public function all();
	public function userRoles();
}