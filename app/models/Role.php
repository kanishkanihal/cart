<?php

class Role extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'roles';
	
	/**
	 * Has many users
	 */
	public function users()
	{
		return $this->belongsToMany('User', 'role_user','role_id','user_id');
	}
}
