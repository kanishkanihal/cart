<?php
use repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\Input;
use cart\validators\UserValidator;
use cart\helper\FileHelper;

class UserController extends \BaseController {

	public $user;
	public $rules;
	
	public function __construct(UserRepositoryInterface $user)
	{
		$this->user = $user;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::paginate(5);
		return View::make('users.index', array('users' =>$users));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$userValidator = new UserValidator($input);
		$validator = $userValidator->onStore();
		if ($validator->fails())
		{
			return Redirect::back()->withInput()->withErrors($validator);
		}
		$this->user->insert($input);
		return Redirect::to('user');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = $this->user->find($id);
		return View::make('users.show', array('user' =>$user));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->user->find($id);
		return View::make('users.edit', array('user' =>$user));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$input = Input::except('image');
		$userValidator = new UserValidator($input);
		$file = new FileHelper();
		
		$validator = $userValidator->onUpdate($id);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator);
		}
		$image = $file->savefile(
				Request::file('image'), 
				Input::get('full_name').'_'.$id,'profile',
				array(array(150,150),array(500,500))
		);
		if($image){
			$input['image'] = $image;
		}
		$this->user->update($id, $input);
		return Redirect::to('user');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = $this->user->find($id);
		$file = new FileHelper();
		$file->removeFile($user->image,'profile',array(array(150,150),array(500,500)));
		$this->user->delete($id);
	}
	
	/**
	 * 
	 */
	public function roles(){
		$user = $this->user->allWithRole();
		return View::make('users.role', array('user' => $user));
	}
	
	/**
	 * 
	 * @param int $id
	 */
	public function roleUpdate($id){
		$roles = Input::get('role');
		$this->user->updateUserRoles($id, $roles);
		return Redirect::to('user/role');
	}


}
