<?php

use repositories\RoleRepositoryInterface;
use repositories\UserRepositoryInterface;

class RoleController extends \BaseController {

	public $role;
	public $user;

	function __construct(RoleRepositoryInterface $role,UserRepositoryInterface $user){
		$this->role = $role;
		$this->user = $user;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = $this->role->all();
		return View::make('roles.index', array('roles' =>$roles));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$role = $this->role->find($id);
		return View::make('roles.show', array('role' =>$role));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$role = $this->role->find($id);
		$user = $this->user->allWithRole($id);
		return View::make('roles.edit', array('role' =>$role,'user' => $user));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
