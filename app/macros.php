<?php
Form::macro('im', function($name,$value,$attribute = array())
{
	if(Form::getValueAttribute('image') != NULL){
		$path = url($value.'150x150_'.Form::getValueAttribute('image'));
	} else {
		$path = url('/img/common/square-image.png');
	}
	$id = isset($attribute['id'])? $attribute['id'] :substr( md5(rand()), 0, 7);
	return
	"<label for='$id' class='initial'>".
	"<input type='file'' id='$id' class='fileupload' name='$name' style='display: none'>".
	"<canvas width='150' height='150' class='image' data-img='$path'></canvas>".
	'</label>';
});