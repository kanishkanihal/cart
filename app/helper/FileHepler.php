<?php
namespace cart\helper;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;


class FileHelper {
	
	private $upload;
	
	public function __construct(){
		$this->upload = Config::get('configvar.upload');
	}

	/**
	 * Remove/delete files/images from the disk
	 * 
	 * @param string $image
	 * @param string $name
	 * @param string $folder
	 * @param array $dimentions
	 * @return boolean|string
	 */
	function savefile($file, $name = NULL,$folder = null,  $dimentions = array()){
		if($file == NULL){
			return false;
		}
		if ($file)
		{
			if($name) {
				$imgName = $name.'.'.$file->getClientOriginalExtension();
			}else {
				$imgName = $file->getClientOriginalName();
			}
			$imgName = strtolower(str_replace(' ', '_', $imgName));
			$img = Image::make($file);
			$path = $this->upload;
			if ($folder != NULL){
				$path = $this->upload.$folder.'/';
			}
			if(!empty($dimentions)){
				foreach ($dimentions as $dimention){
					$width = $dimention[0];
					$height = $dimention[1];
					if ($width != NULL AND $height != NULL){
						$img->fit($width, $height, function ($constraint) {
							$constraint->upsize();
						});
						$img->save($path.$width.'x'.$height.'_'.$imgName);
						$img = Image::make($file);
					}else{
						$img->save($path.$imgName);
					}
				}
			}else {
				$img->save($path.$imgName);
			}
			return $imgName;
		} else {
			return false;
		}
	}

	/**
	 * Remove/delete files/images from the disk
	 * 
	 * @param string $name
	 * @param string $folder
	 * @param arrray $dimentions
	 * @return boolean
	 */
	public function removeFile($name = NULL,$folder = null,  $dimentions = array()){
		if($name == NULL){
			return false;
		}
		$imgName = $name;
		$path = 'public/uploads/';
		if ($folder != NULL){
			$path = 'public/uploads/'.$folder.'/';
		}
		if(!empty($dimentions)){
			foreach ($dimentions as $dimention){
				$width = $dimention[0];
				$height = $dimention[1];
				if ($width != NULL AND $height != NULL){
					unlink($path.$width.'x'.$height.'_'.$imgName);
				}else{
					unlink($path.$imgName);
				}
			}
		}else {
			unlink($path.$imgName);
		}
		return true;
	}
}