<?php
namespace common;

use Illuminate\Pagination\Presenter;

class SemanticUI extends Presenter {
	
	public function getActivePageWrapper($text)
	{
		return '<li class="active item"><a href="">'.$text.'</a></li>';
	}
	
	public function getDisabledTextWrapper($text)
	{
		return '<li class="disabled item"><a href="">'.$text.'</a></li>';
	}
	
	public function getPageLinkWrapper($url, $page, $rel = null)
	{
		return '<li class="item"><a href="'.$url.'">'.$page.'</a></li>';
	}
}