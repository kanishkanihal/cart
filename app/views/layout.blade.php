<html>
	<head>
		<title>Cart</title>
		<link rel="stylesheet" type="text/css" href="{{URL::to('bower_components/semantic-ui/dist/semantic.css')}}">
		<link rel="stylesheet" type="text/css" href="{{URL::to('bower_components/semantic-ui-grid/grid.css')}}">
		<link rel="stylesheet" type="text/css"  href="{{URL::to('bower_components/pikaday/css/pikaday.css')}}" />
<!-- 		<link rel="stylesheet" type="text/css"  href="{{URL::to('bower_components/dropzone/downloads/css/dropzone.css')}}" /> -->
	</head>
    <body>
<div class="ui inverted menu">
  <a class="active item">
    <i class="home icon"></i> Home
  </a>
  <a class="item">
    <i class="mail icon"></i> Messages
  </a>
  <a class="item">
    <i class="user icon"></i> Friends
  </a>
</div>
	<div class="ui grid">
		<div class="row">
		    <div class="three wide column desktop">@section('sidebar')This is the master sidebar. @show</div>
		    <div class="ten wide column">@yield('content')</div>
		    <div class="three wide column"></div>>
		</div>
	</div>
	        <footer>
	        
	        	<script src="{{URL::to('bower_components/jquery/dist/jquery.js')}}"></script>
	        	<script src="{{URL::to('bower_components/semantic-ui/dist/semantic.js')}}"></script>
	            <script src="{{URL::to('bower_components/moment/moment.js')}}"></script>
	        	<script src="{{URL::to('bower_components/pikaday/pikaday.js')}}"></script>
	        	<script src="{{URL::to('bower_components/pikaday/plugins/pikaday.jquery.js')}}"></script>
	        	<script src="{{URL::to('bower_components/jcanvas/jcanvas.min.js')}}"></script>
				<script src="{{URL::to('js/cart.js')}}"></script>
	        	@yield('script')
	        </footer>
    </body>
</html>