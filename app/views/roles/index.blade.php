@extends('layout')

@section('content')
<table class="ui table">
  <thead>
    <tr>
      <th>Role</th>
      <th>Code</th>
      <th class="six wide">Actions</th>
    </tr>
  </thead>
  <tbody>
	@foreach ($roles as $role)
    <tr>
      <td>{{$role->role}}</td>
      <td>{{$role->code}}</td>
      
			<td>
			<div class="">
				{{ link_to_route('role.show', 'View',
				$parameters = array($role->id), 
				$attributes = array('class'=> "ui yellow button"),
				$secure = null); 
				}} 
				{{ link_to_route('role.edit', 'Edit',
				$parameters	= array($role->id), 
				$attributes = array('class'=> "ui blue button"),
				$secure = null); 
				}}
			 </div>
			</td>     
    </tr>
	@endforeach
  </tbody>
</table>
@stop