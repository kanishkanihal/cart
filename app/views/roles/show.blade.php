@extends('layout') @section('content')
<h3 class="ui dividing header">Role Information</h3>
<div class="segment">
	
	<ul class="ui raised segment">
		<li class="ui ribbon label">Role</li>
		<p>{{ $role->role}}</p>
		<li class="ui ribbon label">Code</li>
		<p>{{$role->code}}</p>
		<li class="ui ribbon label">Users</li>
		@foreach ($role->users as $user)
		    <p>{{ $user->email}}</p>
		@endforeach
	</ul>
{{ link_to('role', 'Roles', $attributes = array('class'=>"ui button black"), $secure = null); }}
</div>
@stop

