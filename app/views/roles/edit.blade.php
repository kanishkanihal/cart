@extends('layout') @section('content') 

<h3 class="ui dividing header">Role Information</h3>

{{ Form::model($role, array('route' => array('role.update', $role->id),'files' => true, 'method' => 'PUT', 'class' => 'ui form segment')) }}

@if (count($errors->all()) > 0)
<div class="ui icon red message">
	<i class="close icon"></i>
	<i class="warning sign icon"></i>
	<div class="header">We're sorry</div>
		@foreach ($errors->all() as $message)
		    <p>{{ $message }}</p>
		@endforeach
</div>
@endif
<div class="">
	<div class="two fields">
		<div class="field">
			{{ Form::label('role', 'Role')}}
			<div class="field">
			{{ $role->role }}
			</div>
		</div>
		<div class="field">
			{{ Form::label('code', 'Code')}}
			<div class="field">
			{{ $role->code }}
			</div>
		</div>
	</div>
		@foreach ($user as $u)
		    <p>{{ $u->email }}{{$u->roles}}</p>
		    @foreach ($u->roles as $r)
		    	<li>{{ $r}}</li>
		    @endforeach
		@endforeach
	{{ Form::submit('Update', $attributes = array( 'class' => "ui submit
	blue button" )); }}
	{{ link_to('role', 'Back', $attributes = array('class'=>"ui button black"), $secure = null); }}
</div>

{{ Form::close() }} @stop @section('script')
<script type="text/javascript">
$(document).ready(function() {

});

</script>
@stop

