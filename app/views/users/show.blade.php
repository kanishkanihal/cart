@extends('layout') @section('content')
<h3 class="ui dividing header">User Information</h3>
<div class="segment">
	
	<ul class="ui raised segment">
		<li class="ui ribbon label">Name</li>
		<p>{{ $user->full_name}}</p>
		<li class="ui ribbon label">E-mail</li>
		<p>{{$user->email}}</p>
		<li class="ui ribbon label">Birth day</li>
		<p>{{ $user->dob}}</p>
		<li class="ui ribbon label">Sex</li>
		<p>@if ($user->sex == 1) Female	@else Male @endif</p>  
		<li class="ui ribbon label">Address</li>
		<p>{{ $user->address}}</p> 
		<li class="ui ribbon label">City</li>
		<p>{{ $user->city}}</p> 
		<li class="ui ribbon label">ZIP</li>
		<p>{{ $user->zip}}</p> 
	</ul>
{{ link_to('user', 'User', $attributes = array('class'=>"ui button black"), $secure = null); }}
</div>
@stop

