@extends('layout') @section('content') 

<h3 class="ui dividing header">Accocunt Information</h3>

{{ Form::model($user, array('route' => array('user.update', $user->id),'files' => true, 'method' => 'PUT', 'class' => 'ui form segment')) }}

@if (count($errors->all()) > 0)
<div class="ui icon red message">
	<i class="close icon"></i>
	<i class="warning sign icon"></i>
	<div class="header">We're sorry</div>
		@foreach ($errors->all() as $message)
		    <p>{{ $message }}</p>
		@endforeach
</div>
@endif
<div class="">
	<div class="field">
		{{ Form::label('image', 'Profile picture') }}
	</div>


<div  class="">
    
    {{ Form::im('image','uploads/profile/',array('id' => "profile")); }}   
</div>
<div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
</div>		
	<div class="two fields">
		<div class="field">
			{{ Form::label('username', 'User name')}}
			<div class="ui icon input">
				{{ Form::text('username', $value = null, $attributes = array(
				'placeholder'=>"User name", 'id' => "username" )); }} <i
					class="user icon"></i>
			</div>
		</div>
		<div class="field">
			{{ Form::label('email', 'Email')}}
			<div class="ui icon input">
				{{ Form::email('email', $value = null, $attributes = array(
				'placeholder'=>"E-mail", 'id' => 'email' )); }} <i class="mail icon"></i>
			</div>
		</div>
	</div>
	<div class="field">
		{{ Form::label('password', 'Password')}}
		<div class="two fields">
			<div class="field">
				<div class="ui icon input">
					{{ Form::password('password', $value = null, $attributes = array(
					'placeholder'=>"password", 'id' => 'password' )); }} <i
						class="lock icon"></i>
				</div>
			</div>
			<div class="field">
				<div class="ui icon input">
					{{ Form::password('password_confirmation', $value = null, $attributes =
					array( 'placeholder'=>"Password again", 'id' => 'password_confirmation'
					)); }} <i class="lock icon"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="two fields">
		<div class="field">
			{{ Form::label('dob', 'Date of birth')}}
			<div class="ui icon input">
				{{ Form::text('dob', $value = null, $attributes = array(
				'placeholder'=>"Date of birth", 'class' => "datepicker", 'id' =>
				'dob' )); }} <i class="Calendar icon"></i>
			</div>
		</div>
		<div class="field">
			<label>Sex</label>
			<div class="fields">
				<div class="field">
					<div class="ui radio">{{ Form::radio('sex','0', $attributes =
						array( 'checked' => "checked", 'id' => 'male' )); }} {{
						Form::label('sex', 'Male')}}</div>
				</div>
				<div class="field">
					<div class="ui radio">{{ Form::radio('sex','1', $attributes =
						array( 'checked' => "checked", 'id' => 'female' )); }} {{
						Form::label('', 'Female')}}</div>
				</div>
			</div>
		</div>
	</div>

	<div class="field">
		{{ Form::label('full_name', 'Full name')}}
		<div class="field">
			<div class="ui icon input">{{ Form::text('full_name', $value = null,
				$attributes = array( 'placeholder'=>"Full name", 'id' => 'full_name'
				)); }}</div>
		</div>
	</div>
	<div class="field">
		{{ Form::label('address', 'Address')}}
		<div class="field">
			<div class="ui icon input inline">
				{{ Form::text('address', $value = null, $attributes = array(
				'placeholder'=>"Full address", 'id' => 'address', )); }} <i
					class="home icon"></i>
			</div>
		</div>
	</div>
	<div class="two fields">
		<div class="field">
			{{ Form::label('city', 'City')}}
			<div class="ui icon input">{{ Form::text('city', $value = null,
				$attributes = array( 'placeholder'=>"City name", 'id' => "city" ));
				}}</div>
		</div>
		<div class="field">
			{{ Form::label('zip', 'ZIP')}}
			<div class="ui icon input">{{ Form::text('zip', $value = null,
				$attributes = array( 'placeholder'=>"ZIP code", 'id' => "zip" )); }}
			</div>
		</div>
	</div>
	{{ Form::submit('Update', $attributes = array( 'class' => "ui submit
	blue button" )); }}
	{{ link_to('user', 'Back', $attributes = array('class'=>"ui button black"), $secure = null); }}
</div>

{{ Form::close() }} @stop @section('script')
<script type="text/javascript">
$(document).ready(function() {
	$('.ui.form')
	.form({//v-required
		username: {
	    identifier : 'username',
	    rules: [
		      {
		          type   : 'empty',
		          prompt : 'Please enter your name'
		      }
	    	]
	  	},
	  	email: {
		    identifier : 'email',
		    rules: [
		      {
		        type   : 'email',
		        prompt : 'Please enter a valid e-mail'
		      }
		    ]
		  },
		password: {
		    identifier : 'password',
		    rules: [
		      {
		        type   : 'match[password_confirmation]',
		        prompt : 'Please enter a equal passwords'
		      },
		    ]
		  },
		dob: {
	      identifier : 'dob',
	      rules: [
	        {
	          type   : 'date',
	          prompt : 'Please enter a valied date'
	        }
	      ]
	    },
	    full_name: {
		      identifier : 'full_name',
		      rules: [
		        {
		          type   : 'empty',
		          prompt : 'Please enter the full name'
		        }
		      ]
		 },
		 address: {
		      identifier : 'address',
		      rules: [
		        {
		          type   : 'empty',
		          prompt : 'Please enter the address'
		        }
		      ]
		    },
		 city: {
		      identifier : 'city',
		      rules: [
		        {
		          type   : 'empty',
		          prompt : 'Please enter the city'
		        }
		      ]
		    },
		zip: {
		      identifier : 'zip',
		      rules: [
		        {
		          type   : 'integer',
		          prompt : 'Please enter a valied zip code'
		        }
		      ]
		    },
        on: 'submit',
        inline: 'true',
	});
});

</script>
@stop

