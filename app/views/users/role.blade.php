@extends('layout') @section('content') 

<h3 class="ui dividing header">Role Information</h3>

<table class="ui table">
  <thead>
    <tr>
      <th>User</th>
      <th>Role</th>
	  <th class="four wide">Actions</th>
    </tr>
  </thead>
  <tbody>
		@foreach ($user as $u)
		<tr>
		    <td>{{$u->email}}</td>
		    <td>
		    {{--*/ $suser = '' /*--}}
		    {{--*/ $auser = '' /*--}}
		    {{--*/ $uuser = '' /*--}}

		    @foreach ($u->roles as $r)
				@if ($r->role_id == 1)
				    {{--*/ $suser = 1 /*--}}
				@elseif ($r->role_id == 2)
				    {{--*/ $auser = 2 /*--}}
				@elseif ($r->role_id == 3)
				    {{--*/ $uuser = 3 /*--}}
				@endif
		    @endforeach
		    {{Form::open(array('route' => array('user.role_update', $u->id)))  }}
		    
		    {{ Form::checkbox('role[]', '1', $suser,array('id'=> $u->id.'_sa')) }}
		    {{ Form::label($u->id.'_sa', 'Super admin')}}
		    
		    {{ Form::checkbox('role[]', '2', $auser,array('id'=> $u->id.'_a')) }}
		    {{ Form::label($u->id.'_a', 'Admin')}}
		    
		    {{ Form::checkbox('role[]', '3', $uuser,array('id'=> $u->id.'_u')) }}
		    {{ Form::label($u->id.'_u', 'User')}}

		    </td>
			<td>
			<div class="">
			{{ Form::submit('Update', $attributes = array( 'class' => "ui submit
				blue button" )); }}
			 </div>
			 {{ Form::close() }}
			</td> 
		</tr>
		@endforeach
  </tbody>
</table>
{{ $user->links(); }}
@stop 
@section('script')
<script type="text/javascript">
$(document).ready(function() {

});

</script>
@stop

