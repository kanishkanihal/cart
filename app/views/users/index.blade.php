@extends('layout') @section('content')
<h3 class="ui dividing header">User list</h3>
{{ link_to('user/create', 'Add User', $attributes = array('class'=>"ui button green"), $secure = null); }}
<table class="ui striped table">
	<thead>
		<tr>
			<th class="six wide">Name</th>
			<th class="four wide">E-mail</th>
			<th class="six wide">Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($users as $user)
		<tr>
			<td>{{$user->full_name}}</td>
			<td>{{$user->email}}</td>
			<td>
			<div class="">
				{{ link_to_route('user.show', 'View',
				$parameters = array($user->id), 
				$attributes = array('class'=> "ui yellow button"),
				$secure = null); 
				}} 
				{{ link_to_route('user.edit', 'Edit',
				$parameters	= array($user->id), 
				$attributes = array('class'=> "ui blue button"),
				$secure = null); 
				}}
				{{ link_to_route('user.destroy', 'Delete',
				$parameters	= array($user->id), 
				$attributes = array('class'=> "ui red button delete"),
				$secure = null); 
				}}
			 </div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $users->links(); }}

@stop
@stop @section('script')
<script type="text/javascript">
$(document).ready(function() {
	$(".delete").on('click',function(){
	   if (confirm("Press a button!") == true) {
			$.ajax({
				url: $(this).attr('href'),
				type: 'post',
				data: {_method: 'delete',},
				success:function(msg){
					location.reload();
				}
			});
			return false;
	    } else {
	        alert("You pressed Cancel!");
	    }

	});
});

</script>
@stop