<?php
namespace cart\validators;

use Illuminate\Support\Facades\Validator;

class UserValidator{

	public $rules = array(
		'email' => 'required|email',
		'username' => 'required',
		'password' => 'min:3|confirmed',
		'full_name' => 'required',
		'address' => 'required',
		'city' => 'required',
		'dob' => 'required',
		'zip' => 'required|digits:5',
	);

	public $data;
	
	public function __construct($data)
	{
		$this->data = $data;
	}
	
	public function onUpdate($id){
		$this->rules['email'] = 'required|email|unique:users,email,'.$id;
		$validator = Validator::make($this->data, $this->rules);
		return $validator;		
	}
	
	public function onStore(){
		$this->rules['password'] = 'required|min:3|confirmed';
		$this->rules['email'] = 'required|email|unique:users';
		$validator = Validator::make($this->data, $this->rules);
		return $validator;
	}
	
	
}