<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
		    $table->increments('id');
		    $table->string('email');
		    $table->string('username');
		    $table->string('password');
		    $table->string('address');
		    $table->string('city');
		    $table->integer('zip');
		    $table->integer('country');
		    $table->date('dob');
		    $table->enum('sex', array('0', '1'));
		    $table->timestamps();
		    
		    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}

}
