<?php
class RoleTableSeeder extends Seeder {

	public function run()
	{
		DB::table('roles')->delete();

		Role::create(array('role' => 'Super admin', 'code' => 'SUPER_ADMIN'));
		Role::create(array('role' => 'Admin', 'code' => 'ADMIN'));
		Role::create(array('role' => 'Uesr', 'code' => 'USER'));
	}

}