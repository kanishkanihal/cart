## Thanks to
* Laravel4
* Bower
* Semantic UI
* HTML5

##Instalation
1. git pull the repository.
2. Execute composer update to update my vendor folder.
3. Go to /public folder, Execute bower update/install to download whatever js or css I've installed.
4. Create /public/uploads folder.
5. Execute php artisan migrate to upgrade my database schema.
6. Execute chmod and chown to fix whatever permission mess those commands might have made to my directories while downloading files.

##Live demo
[cart.kanaslab.com/user](http://cart.kanaslab.com/user)
