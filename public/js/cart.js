var MAX_FILE_SIZE = 2;//File size in MB

$('.ui.radio.checkbox').checkbox();

$('.datepicker').pikaday({ firstDay: 0,format: 'YYYY-MM-DD' });

$.fn.form.settings.rules.date = function(value){
      var dateRegExp = 
	      new RegExp("[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])", "i");
      return dateRegExp.test(value);
}//inline: 'true',

$.fn.form.settings.inline = 'true';

$('.message .close').on('click', function() {
	  $(this).closest('.message').fadeOut();
});

$(".fileupload").change(function(e) {
	var input = $(this)[0];
	var sbl = $(this).siblings('canvas');
	if (input.files && input.files[0]) {
		var tempT = input.files[0].type.split("/");
		var tempS = input.files[0].size/(1024*1024);
		if (tempS < MAX_FILE_SIZE) {
			var validFileExtensions = ["jpg", "jpeg" , "gif", "png"];
			if (tempT[0] === 'image') { 
				if($.inArray( tempT[1], validFileExtensions ) >= 0){
					var reader = new FileReader();
					reader.onload = function(e) {
						//http://www.codeforest.net/html5-image-upload-resize-and-crop
						var tempImg = new Image();
						tempImg.src = reader.result;
						tempImg.onload = function() {
							var MAX_WIDTH = sbl.attr('width');
							var MAX_HEIGHT = sbl.attr('height');
							var x_pos = 0;
							var y_pos = 0;
			
							var tempW = tempImg.width;
							var tempH = tempImg.height;
							if (tempW > tempH) {
								if (tempW > MAX_WIDTH) {
									tempW *= MAX_HEIGHT / tempH;
									tempH = MAX_HEIGHT;
									var x_pos = (tempW - MAX_WIDTH) / 2 * (-1);
								}
							} else {
								if (tempH > MAX_HEIGHT) {
									tempH *= MAX_WIDTH / tempW;
									tempW = MAX_WIDTH;
									var y_pos = (tempH - MAX_HEIGHT) / 2 * (-1);
								}
							}
							sbl.clearCanvas();
							sbl.drawImage({
								source : tempImg.src,
								width : tempW,
								height : tempH,
								fromCenter : false,
								fillStyle : 'red',
								layer : true,
								x : (x_pos),
								y : (y_pos),
							});
						}
					}
					var start = 0;
					reader.onprogress = function(e) {
						sbl.drawArc({
							  strokeStyle: 'white',
							  strokeWidth: 5,
							  x: 75, y: 75,
							  radius: 50,
							  // start and end angles in degrees
							  start: start, end: (e.loaded / e.total * 360)
								
							});
						start = e.loaded / e.total * 360;
						console.log(e.loaded / e.total * 360);
					}
					reader.readAsDataURL(input.files[0]);
				}else {
					alert("file extention must be one of \".jpg\", \".jpeg\", \".bmp\", \".gif\", \".png\"");
					input.empty();
				}
			}else{
				alert("upload an image");
				input.empty();
			}
		} else {
			alert ("file is too large");
			input.empty();
		}
	}
});

$("canvas").each(function(){
    $(this).drawImage({source: $(this).data('img'),  x: 75, y: 75}); 
});